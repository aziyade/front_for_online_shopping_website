import { Component, OnInit } from '@angular/core';
import {ProductService} from "../product/product.service";
import {HttpClient} from "@angular/common/http";
import {Validators, FormBuilder} from "@angular/forms";
import {Product} from "../product/product.component";

@Component({
  selector: 'app-product-form',
  templateUrl: './product-form.component.html',
  styleUrls: ['./product-form.component.css']
})
export class ProductFormComponent implements OnInit {


  productForm = this.formBuilder.group({
    name: ['', Validators.required],
    price: ['']

  })
  constructor(private formBuilder: FormBuilder, private http: HttpClient,
             private productService: ProductService) { }

  ngOnInit(): void {
  }

  onSubmit() {
    let product:Product = this.productForm.value;
    this.productService.addProduct(product).subscribe();

    console.warn(this.productForm.value);
  }

  updateProduct() {
    this.productForm.patchValue({
      name: 'Poire',
      price: 8,
    });
  }

}
