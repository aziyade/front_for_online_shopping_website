import {Component, OnInit} from '@angular/core';
import {Observable} from "rxjs";
import {Product} from "../product/product.component";
import {ProductService} from "../product/product.service";
import {ActivatedRoute} from "@angular/router";
import {HttpClient} from "@angular/common/http";
import {environment} from "../../environments/environment";

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.css']
})
export class ProductListComponent implements OnInit {
  products$!: Observable<Product[]>;


  constructor(private productService: ProductService,
              private route: ActivatedRoute, private http: HttpClient) {
  }


  ngOnInit(): void {
    this.products$ = this.productService.getAllProducts();
  }

  deleteProduct(id: number): void {
    this.productService.deleteProduct(id).subscribe(() => {
      this.products$ = this.productService.getAllProducts();
      return this.products$;
    });

    /*
    (() => {
      this.products$ = this.productService.getAllProducts();
      return this.products$;
    });

    cette lamba ci dessus(dans le delete) permet de reload la liste sans reload toute la page
     */
  }


}
