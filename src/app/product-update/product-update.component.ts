import { Component, OnInit } from '@angular/core';
import {Observable} from "rxjs";
import {Product} from "../product/product.component";
import {ProductService} from "../product/product.service";
import {ActivatedRoute} from "@angular/router";
import {HttpClient} from "@angular/common/http";
import {FormBuilder, Validators} from "@angular/forms";

@Component({
  selector: 'app-product-update',
  templateUrl: './product-update.component.html',
  styleUrls: ['./product-update.component.css']
})

export class ProductUpdateComponent implements OnInit {
product$: Observable<Product> | undefined;

productUpdate = this.formBuilder.group({
    name: ['', Validators.required],
    price: ['']

  })

  constructor(private formBuilder: FormBuilder, private http: HttpClient,
              private productService: ProductService, private route: ActivatedRoute) { }



  ngOnInit(): void {
    const routeParam = this.route.snapshot.paramMap;
    const productId = Number(routeParam.get("id"));
    this.product$ = this.productService.getProductById(productId);
    this.product$.subscribe(p => {
      console.log(p)
      this.productUpdate.patchValue(p)
    });

  }


  updateProduct() {

    let product:Product = this.productUpdate.value;
    product.productId =  Number(this.route.snapshot.paramMap.get("id"));
    this.productService.updateProduct(product).subscribe();

    console.warn(this.productUpdate.value);
  }


}
