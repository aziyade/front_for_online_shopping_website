import { Component, OnInit } from '@angular/core';
import {Observable} from "rxjs";
import {Product} from "../product/product.component";
import {ProductService} from "../product/product.service";
import {ActivatedRoute} from "@angular/router";

@Component({
  selector: 'app-product-detail',
  templateUrl: './product-detail.component.html',
  styleUrls: ['./product-detail.component.css']
})
export class ProductDetailComponent implements OnInit {
product$: Observable<Product> | undefined;



  constructor(private productService: ProductService,
              private route: ActivatedRoute) { }

  ngOnInit(): void {
    const routeParam = this.route.snapshot.paramMap;
    const productId = Number(routeParam.get("id"));
    this.product$ = this.productService.getProductById(productId);
    this.product$.subscribe(p => console.log(p));
  }

}
