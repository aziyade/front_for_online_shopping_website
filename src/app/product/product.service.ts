import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Product} from "./product.component";
import {Observable} from "rxjs";
import {environment} from "../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class ProductService {
  url = environment.url + '/products';

  constructor(private http: HttpClient) {

  }



  getAllProducts() {
    return this.http.get<Product[]>(this.url);
  }

  getProductById(id: number) {
    return this.http.get<Product>(this.url + '/' + id);
  }

  addProduct(product: Product) {
    return this.http.post(this.url, product);
  }

  deleteProduct(id: number): Observable<Product> {
    return this.http.delete<Product>(this.url  + '/' + id);

  }

  updateProduct(product: Product): Observable<Product> {
    return this.http.put<Product>(this.url  + '/' + product.productId, product);
  }
}
