import { Component, OnInit } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {ProductService} from "./product.service";
import {Observable} from "rxjs";


export interface Product {
  productId: number;
  name: string;
  price: number;
}

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})

export class ProductComponent implements OnInit {
  product$!: Observable<Product[]>;

  constructor(private http:HttpClient, private productService: ProductService) {
  }

  ngOnInit(): void {
    this.product$ = this.productService.getAllProducts();
  }

  delete(id: number) : void {
    this.productService.deleteProduct(id).subscribe(() => {
      this.product$ = this.productService.getAllProducts();
    })

  }

}
